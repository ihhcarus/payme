### Publicação

Para disponibilizar uma nova versão da biblioteca, incremente a versão no arquivo `setup.py` e execute o comando abaixo.
```
python setup.py sdist --formats=gztar,zip
```
Em seguida, crie uma tag no repositório no commit desejado.

### Instalação

Instale usando o seguinte comando do `pip` (é necessário ter acesso de leitura ao repositório privado).

```
pip install git+ssh://git@bitbucket.org/ihhcarus/payme.git/@0.4.0 --process-dependency-links
# Se rolar um erro de depreciação da flag não tem problema por enquanto
```

### Configuração básica

As seguintes constantes são necessários nos settings da aplicação.

```python
# Para PagSeguro
PAGSEGURO_API_EMAIL = 'seuemail@email.com'  # email de acesso à API do PagSeguro
PAGSEGURO_API_TOKEN = 'L3H41LLSHJ23L4JHF23'  # token de acesso à API do PagSeguro
PAGSEGURO_IS_SANDBOX = True  # flag que indica se deve ser utilizado o ambiente de sandbox da API do PagSeguro
```

### Integração com Django

Adicione aos seus `INSTALLED_APPS` as plataformas que quiser usar. Se deseja usar os templates, adicione o nível `django` do pacote.

```python
INSTALLED_APS = [
    # ...
    'platforms.pagseguro',
    'platforms.pagseguro.django',  # Para usar os templates de Django
]
```

Adicione o caminho dos templates de cada plataforma que quiser usar aos `DIRS` de `TEMPLATES`.

```python
TEMPLATES = [
    {
        # ...
        'DIRS': [
            os.path.join(BASE_DIR, 'platforms', 'pagseguro', 'django', 'templates')
        ],
        # ...
    }
```


Nas suas views, herde as classes da plataforma que quiser usar que sempre estão em `platforms.[nome].django.mixins`.

```python
from platforms.pagseguro.django.mixins import TransactionMixin

class MyTransactionView(TransactionMixin, TemplateView):
    pass  # Leia a documentação da classe para ver as funcionalidades
```

Cada plataforma pode fornecer diferentes templates que facilitam a integração, como botões e formulários. Estes estão localizados em `platforms.[nome].django.templates.[nome]`.

```html
<body>
    <!-- Inclui o botão de pagamento com lightbox do PagSeguro -->
    {% include "pagseguro/button/lightbox/payment.html" %}
    <!-- Inclui o botão de recorrência com redirecionamento do PagSeguro -->
    {% include "pagseguro/button/default/pre-approval.html" %}
</body>
```

Veja os exemplos de uso na pasta `samples`.
