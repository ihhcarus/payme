#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages


def readme():
    u""" Lê o arquivo README pra incluir no pacote do módulo. """
    with open('README.md') as f:
        return f.read()


setup(
    name='payme',
    version='0.4.9',
    description=u"Facilita integração de projetos em Python com plataformas de pagamento.",
    long_description=readme(),
    author=u"Ícaro Raupp",
    author_email='budicaro@gmail.com',
    url='https://bitbucket.org/ihhcarus/payme',
    packages=find_packages(exclude=['samples*']),
    install_requires=[
        'pagseguro==0.4.7',
    ],
    dependency_links=[
        'git+https://github.com/ihhcarus/python-pagseguro.git@compat#egg=pagseguro-0.4.7',
    ],
    license='MIT',
    include_package_data=True,
    zip_safe=False
)
