# -*- coding: utf-8 -*-


from __future__ import absolute_import  # Deixa esse cara aqui pra não zicar o import de módulos com mesmo nome!

from django.apps import AppConfig


class PlatformPagSeguroConfig(AppConfig):
    name = 'platforms.pagseguro'
    label = 'platforms_pagseguro'
    verbose_name = u"PagSeguro"
