# -*- coding: utf-8 -*-


from django import template


register = template.Library()


@register.filter(name='get_expiry_date')
def get_expiry_date_filter(payment_order, default_period=None):
    u"""
    Buscamos a data que a validade deste pagamento expira.
    
    :param payment_order: ordem de pagamento para se calcular a data de validade
    :type payment_order: platforms.pagseguro.django.models.pre_approval.PaymentOrder
    :param default_period: quantidade de dias padrão para usar caso período não esteja disponível
    :type default_period: int
    :return: data de expiração, se disponível
    :rtype: datetime | None
    """
    return payment_order.get_expiry_date(default_period)
