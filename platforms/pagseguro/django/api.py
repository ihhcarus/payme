# -*- coding: utf-8 -*-


import random
import string

from django.conf import settings
from pagseguro_v2 import PagSeguro
from platforms.pagseguro.api.models import PagSeguro as PagSeguroPayme

from platforms.pagseguro.api.api import \
    search_transactions_by_date_range as api_search_transactions_by_date_range, \
    search_transactions_by_reference as api_search_transactions_by_reference, \
    search_transaction_by_code as api_search_transaction_by_code, \
    search_subscriptions_by_date_range as api_search_subscriptions_by_date_range, \
    search_subscription_by_code as api_search_subscription_by_code, \
    cancel_subscription as api_cancel_subscription, \
    get_subscriptions_payment_orders as api_get_subscriptions_payment_orders
from platforms.pagseguro.api.models import PagSeguro as PagSeguroModel
from platforms.pagseguro.django.models.base import Transaction
from platforms.pagseguro.django.models.checkout import Checkout, CheckoutTransaction
from platforms.pagseguro.django.models.pre_approval import PreApproval, PreApprovalTransaction, Subscription, PaymentOrder


def random_reference(length=200):
    u""" Gera um código aleatório, por padrão do tamanho máximo do código de referência. """
    return ''.join(random.choice(string.ascii_uppercase + string.digits + string.digits) for _ in range(length))


def create_checkout(items, shipping=None, redirect_url=None, max_uses=None, max_age=None, reference=None):
    u"""
    Cria um pagamento de um produto que pode ser comprado por um usuário.

    :param shipping: informações de entrega
    :type shipping: platforms.pagseguro.api.models.Shipping
    :param items: informações sobre os itens do pagamento que será criado
    :type items: list[platforms.pagseguro.api.models.CheckoutItem] | platforms.pagseguro.api.models.CheckoutItem
    :param redirect_url: URL que o PagSeguro vai redirecionar após o pagamento
    :type redirect_url: str
    :param max_uses: número máximo de utilizações deste checkout
    :type max_uses: int
    :param max_age: tempo máximo (em segundos) que este checkout fica disponível
    :type max_age: int
    :param reference: código de referência completo do pagamento
    :type reference: str
    :return: instância do checkout criado
    :rtype: platforms.pagseguro.django.models.checkout.Checkout
    """
    if not reference:
        reference = random_reference()
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True), 'REFERENCE_PREFIX': '%s'}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    pg.reference = reference
    pg.max_age = max_age
    pg.redirect_url = redirect_url
    pg.max_uses = max_uses
    if not isinstance(items, list):
        items = [items]
    for item in items:
        pg.add_item(**vars(item))
    if shipping is not None:
        pg.shipping = vars(shipping)
    response = pg.checkout()
    if response.errors:
        raise ValueError(response.errors)
    elif not getattr(response, 'code', None):
        raise ValueError(u"Checkout was not created: %s" % response.xml)
    else:
        return Checkout.create(response, items, shipping, redirect_url, max_uses, reference, max_age)


def create_pre_approval(pre_approval, redirect_url=None, max_uses=None, reference=None):
    u"""
    Cria uma recorrência que poderá ser assinada por um usuário.

    :param redirect_url: URL que o PagSeguro vai redirecionar após o pagamento
    :type redirect_url: str
    :param max_uses: número máximo de utilizações deste checkout
    :type max_uses: int
    :param pre_approval: informações sobre a recorrência que será criada
    :type pre_approval: platforms.pagseguro.api.models.PreApproval
    :param reference: código de referência completo do pagamento
    :type reference: str
    :return: instância da recorrência criada
    :rtype: platforms.pagseguro.django.models.pre_approval.PreApproval
    """
    if not reference:
        reference = random_reference()
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True), 'REFERENCE_PREFIX': '%s'}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    pg.reference = reference
    pg.redirect_url = redirect_url
    pg.max_uses = max_uses
    pg.pre_approval = vars(pre_approval)
    response = pg.pre_approval_ask_payment()
    if response.errors:
        raise ValueError(response.errors)
    elif not getattr(response, 'code', None):
        raise ValueError(u"Pre Approval was not created: %s" % response.xml)
    else:
        return PreApproval.create(response, pre_approval, redirect_url, max_uses, reference)


def search_transactions_by_reference(reference):
    u"""
    Buscamos no PagSeguro todas as transações com o nosso código de referência que ainda não foram persistidas no banco.

    :param reference: código da referência completo (prefixo e corpo)
    :type reference: str
    :return: tupla com uma lista de novas transações de checkout e uma lista de novas transações de recorrência
    :rtype: (list[platforms.pagseguro.django.models.checkout.CheckoutTransaction], list[platforms.pagseguro.django.models.checkout.PreApprovalTransaction])
    """
    # TODO: Começamos do fim porque a API do PagSeguro é burra e deixa as mais antigas primeiro... (desfazer se consertarem)
    reference_transactions = api_search_transactions_by_reference(reference)[::-1]
    new_checkout_transactions, new_pre_approval_transactions = [], []
    for transacao_pagseguro in reference_transactions:
        transaction_code = transacao_pagseguro.get('code')
        transaction, created = Transaction.get_or_create_from_base_transaction(transaction_code)
        if created and transaction.eh_checkout():
            new_checkout_transactions.append(transaction)
        elif created and transaction.eh_pre_approval():
            new_pre_approval_transactions.append(transaction)
    return new_checkout_transactions, new_pre_approval_transactions


def search_transactions_by_date_range(initial_date, final_date):
    u"""
    Buscamos no PagSeguro todas as transações dentro de uma data inicial e final que ainda não foram persistidas no banco.

    :param initial_date: data de início da busca (até 6 meses atrás)
    :type initial_date: datetime.datetime
    :param final_date: data de fim da busca (até 30 dias da data inicial)
    :type final_date: datetime.datetime
    :return: tupla com uma lista de novas transações de checkout e uma lista de novas transações de recorrência
    :rtype: (list[platforms.pagseguro.django.models.checkout.CheckoutTransaction], list[platforms.pagseguro.django.models.checkout.PreApprovalTransaction])
    """
    reference_transactions = api_search_transactions_by_date_range(initial_date, final_date)
    new_checkout_transactions, new_pre_approval_transactions = [], []
    for transacao_pagseguro in reference_transactions:
        transaction_code = transacao_pagseguro.get('code')
        transaction, created = Transaction.get_or_create_from_base_transaction(transaction_code)
        if created and transaction.eh_checkout():
            new_checkout_transactions.append(transaction)
        elif created and transaction.eh_pre_approval():
            new_pre_approval_transactions.append(transaction)
    return new_checkout_transactions, new_pre_approval_transactions


def update_pending_transactions():
    u"""
    Verificamos sobre todas as transações pendentes se alguma mudou de situação para paga ou cancelada.

    :return: tupla com uma lista de novas transações marcadas como paga ou cancelada de checkout e uma lista de transações marcadas como paga de recorrência
    :rtype: (list[platforms.pagseguro.django.models.checkout.CheckoutTransaction], list[platforms.pagseguro.django.models.checkout.PreApprovalTransaction])
    """
    updated_checkout_transactions, updated_pre_approval_transactions = [], []
    for transaction in CheckoutTransaction.pending.all().iterator():
        pagseguro_transaction = api_search_transaction_by_code(transaction.code)
        if pagseguro_transaction:
            if PagSeguroModel.is_paid(pagseguro_transaction):
                transaction.set_is_paid()
                updated_checkout_transactions.append(transaction)
            elif PagSeguroModel.is_canceled(pagseguro_transaction):
                transaction.set_is_canceled()
                updated_checkout_transactions.append(transaction)
    for transaction in PreApprovalTransaction.pending.all().iterator():
        pagseguro_transaction = api_search_transaction_by_code(transaction.code)
        if pagseguro_transaction:
            if PagSeguroModel.is_paid(pagseguro_transaction):
                updated_pre_approval_transactions.append(transaction)
                transaction.set_is_paid()
            elif PagSeguroModel.is_canceled(pagseguro_transaction):
                updated_pre_approval_transactions.append(transaction)
                transaction.set_is_canceled()
    return updated_checkout_transactions, updated_pre_approval_transactions


def search_subscriptions_by_date_range(initial_date, final_date):
    u"""
    Buscamos no PagSeguro todas as assinaturas dentro de uma data inicial e final que ainda não foram persistidas no banco.

    :param initial_date: data de início da busca (até 6 meses atrás)
    :type initial_date: datetime.datetime
    :param final_date: data de fim da busca (até 30 dias da data inicial)
    :type final_date: datetime.datetime
    :return: lista de novas assinaturas
    :rtype: list[platforms.pagseguro.django.models.pre_approval.Subscription]
    """
    range_subscriptions = api_search_subscriptions_by_date_range(initial_date, final_date)
    new_subscriptions = []
    for subscription_pagseguro in range_subscriptions:
        subscription_code = subscription_pagseguro.get('code')
        subscription, created = Subscription.get_or_create_from_base_subscription(subscription_code)
        if created:
            new_subscriptions.append(subscription)
    return new_subscriptions


def update_pending_subscriptions():
    u"""
    Verificamos sobre todas as assinaturas pendentes se alguma mudou de situação para ativa.

    :return: lista de novas assinaturas marcadas como ativas
    :rtype: list[platforms.pagseguro.django.models.pre_approval.Subscription]
    """
    updated_subscriptions = []
    for subscription in Subscription.pending.all().iterator():
        pagseguro_subscription = api_search_subscription_by_code(subscription.code)
        if pagseguro_subscription and PagSeguroModel.is_active(pagseguro_subscription):
            subscription.set_is_active()
            updated_subscriptions.append(subscription)
    return updated_subscriptions


def update_active_subscriptions():
    u"""
    Verificamos sobre todas as assinaturas ativas se alguma mudou de situação para cancelada.

    :return: lista de novas assinaturas macadas como canceladas
    :rtype: list[platforms.pagseguro.django.models.pre_approval.Subscription]
    """
    canceled_subscriptions = []
    for subscription in Subscription.active.all().iterator():
        pagseguro_subscription = api_search_subscription_by_code(subscription.code)
        if pagseguro_subscription and PagSeguroModel.is_canceled(pagseguro_subscription):
            subscription.set_is_canceled(pagseguro_subscription.status)
            canceled_subscriptions.append(subscription)
    return canceled_subscriptions


def cancel_subscription(subscription):
    u"""
    Cancela uma assinatura.

    :param subscription: assinatura para ser cancelada
    :type subscription: platforms.pagseguro.django.models.pre_approval.Subscription
    :return: se o cancelamento foi um sucesso ou não
    :rtype: bool
    """
    response = api_cancel_subscription(subscription.code)
    if response.canceled:
        pagseguro_subscription = api_search_subscription_by_code(subscription.code)
        if pagseguro_subscription and PagSeguroModel.is_canceled(pagseguro_subscription):
            subscription.set_is_canceled(pagseguro_subscription.status)
            return True
    return False


def get_subscriptions_payment_orders(code):
    u"""
    Buscamos no PagSeguro todas as ordens de pagamento para uma assinatura.

    :param code: código da assinatura para buscar as ordens de pagamento
    :type code: str
    :return: lista de novas ordens de pagamento da assinatura
    :rtype: list[platforms.pagseguro.django.models.pre_approval.PaymentOrder]
    """
    payment_orders = api_get_subscriptions_payment_orders(code)
    new_payment_orders = []
    for payment_order_pagseguro in payment_orders:
        payment_order_code = payment_order_pagseguro.get('code')
        payment_order, created = PaymentOrder.get_or_create_from_base_payment_order(code, payment_order_code, payment_order_pagseguro)
        if created:
            new_payment_orders.append(payment_order)
    return new_payment_orders


def update_subscriptions_payment_orders(code, limit_date=None):
    u"""
    Atualiza o status de todas as ordens de pagamento para uma assinatura.

    :param code: código da assinatura para buscar as ordens de pagamento
    :type code: str
    :param limit_date: data limite opcional para processar as ordens de pagamento
    :type limit_date: datetime.datetime
    :return: lista de novas ordens de pagamento da assinatura
    :rtype: list[platforms.pagseguro.django.models.pre_approval.PaymentOrder]
    """
    payment_orders = api_get_subscriptions_payment_orders(code)
    updated_payment_orders = []
    for payment_order_pagseguro in payment_orders[::-1]:  # Ordena para as mais recentes primeiro
        # Verifica se deve validar a data
        if limit_date is not None:
            scheduling_date = PagSeguroPayme.parse_date(payment_order_pagseguro.get('schedulingDate'))
            if scheduling_date < limit_date:
                break
        payment_order_code = payment_order_pagseguro.get('code')
        payment_order, created = PaymentOrder.update_from_base_payment_order(payment_order_code, payment_order_pagseguro)
        if created:
            updated_payment_orders.append(payment_order)
    return updated_payment_orders
