# -*- coding: utf-8 -*-


from django.conf import settings
from django.shortcuts import redirect
from django.views.generic.base import ContextMixin
from django.views.generic.edit import DeleteView

from platforms.pagseguro.api.api import search_transaction_by_code, search_subscription_by_code
from platforms.pagseguro.django.api import cancel_subscription
from platforms.pagseguro.django.models.pre_approval import Subscription


class PaymentAndPreApprovalMixin(ContextMixin):
    u""" Fornece o código para pagamento e recorrência para utilização dos botões de pagamento do PagSeguro. """

    payment_code = None
    u""" Código de um pagamento. """
    pre_approval_code = None
    u""" Código de uma recorrência. """

    def get_context_data(self, **kwargs):
        u""" Adiciona o código de pagamento e de recorrência no contexto junto com a flag de sandbox. """
        context_data = super(PaymentAndPreApprovalMixin, self).get_context_data(**kwargs)
        context_data['is_sandbox'] = getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)
        context_data['payment_code'] = self.payment_code
        context_data['pre_approval_code'] = self.pre_approval_code
        return context_data


class TransactionMixin(ContextMixin):
    u""" Fornece as informações de uma transação do PagSeguro através das queries da URL. """

    DEFAULT_TRANSACTION_URL_QUERY = 'transaction'

    transaction_url_query = DEFAULT_TRANSACTION_URL_QUERY
    u""" Nome da query que terá o identificador da transação. """
    transaction = None
    u"""
        Objeto da transação que será fornecido.
        :type: pagseguro_v2.parsers.PagSeguroNotificationResponse
    """

    def on_transaction(self, transaction_id):
        u""" Utiliza a API do PagSeguro para buscar uma transação pelo seu identificador. """
        self.transaction = search_transaction_by_code(transaction_id)

    def get(self, request, *args, **kwargs):
        transaction_id = request.GET.get(self.transaction_url_query)
        if transaction_id:
            self.on_transaction(transaction_id)
        return super(TransactionMixin, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        u""" Adiciona a transação buscada ao contexto. """
        context_data = super(TransactionMixin, self).get_context_data(**kwargs)
        context_data['transaction'] = self.transaction
        return context_data


class TransactionFromReferenceMixin(TransactionMixin):
    u""" Valida que a transação buscada é válida a partir de um prefixo e código de referência. """

    reference_prefix = '%s'
    u""" Prefixo de código de referência permitido. Caso não indicado, aceita qualquer um. """
    reference = ''
    u""" Código de referência permitido. Caso não indicado, aceita qualquer um. """

    def on_transaction(self, transaction_id):
        u""" Utiliza a API do PagSeguro para buscar uma transação pelo seu identificador e valida o código de referência. """
        transaction = search_transaction_by_code(transaction_id)
        # Garantimos que a referência começa com o prefixo e termina com o código
        if transaction and \
                transaction.reference and \
                transaction.reference.startswith(self.reference_prefix % '') and transaction.reference.endswith(self.reference):
            self.transaction = transaction


class SubscriptionMixin(ContextMixin):
    u""" Fornece as informações de uma assinatura do PagSeguro através das queries da URL. """

    DEFAULT_SUBSCRIPTION_URL_QUERY = 'code'

    subscription_url_query = DEFAULT_SUBSCRIPTION_URL_QUERY
    u""" Nome da query que terá o identificador da assinatura. """
    subscription = None
    u"""
        Objeto da assinatura que será fornecido.
        :type: pagseguro_v2.parsers.PagSeguroPreApproval
    """

    def on_subscription(self, subscription_id):
        u""" Utiliza a API do PagSeguro para buscar uma assinatura pelo seu identificador. """
        self.subscription = search_subscription_by_code(subscription_id)

    def get(self, request, *args, **kwargs):
        subscription_id = request.GET.get(self.subscription_url_query)
        if subscription_id:
            self.on_subscription(subscription_id)
        return super(SubscriptionMixin, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        u""" Adiciona a assinatura buscada ao contexto. """
        context_data = super(SubscriptionMixin, self).get_context_data(**kwargs)
        context_data['subscription'] = self.subscription
        return context_data


class SubscriptionFromReferenceMixin(SubscriptionMixin):
    u""" Valida que a assinatura buscada é válida a partir de um prefixo e código de referência. """

    reference_prefix = '%s'
    u""" Prefixo de código de referência permitido. Caso não indicado, aceita qualquer um. """
    reference = ''
    u""" Código de referência permitido. Caso não indicado, aceita qualquer um. """

    def on_subscription(self, transaction_id):
        u""" Utiliza a API do PagSeguro para buscar uma assinatura pelo seu identificador e valida o código de referência. """
        subscription = search_subscription_by_code(transaction_id)
        # Garantimos que a referência começa com o prefixo e termina com o código
        if subscription and \
                subscription.reference and \
                subscription.reference.startswith(self.reference_prefix % '') and subscription.reference.endswith(self.reference):
            self.subscription = subscription


class SubscriptionCancelView(DeleteView):
    u""" Facilita o cancelamento de uma assinatura. """

    model = Subscription

    def on_canceled(self):
        u""" Se cancelou com sucesso. """
        return redirect(self.get_success_url())

    def on_not_canceled(self):
        u""" Se não conseguiu cancelar. """
        return redirect(self.get_success_url())

    def delete(self, request, *args, **kwargs):
        u""" Sobrescrevemos o delete pra fazer o cancelamento. """
        self.object = self.get_object()
        if cancel_subscription(self.object):
            return self.on_canceled()
        else:
            return self.on_not_canceled()
