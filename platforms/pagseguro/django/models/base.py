# -*- coding: utf-8 -*-


from django.db import models
from django.db import transaction

from platforms.pagseguro.api.api import search_transaction_by_code
from platforms.pagseguro.api.models import PagSeguro
from platforms.pagseguro.django.managers import TransactionPaidManager
from platforms.pagseguro.django.managers import TransactionPendingManager


class Timestamp(models.Model):
    created = models.DateTimeField(u"Criado em", auto_now_add=True)
    updated = models.DateTimeField(u"Alterado em", auto_now=True)

    class Meta:
        abstract = True


class Customer(Timestamp):
    email = models.EmailField(u"Email", max_length=191, unique=True)  # Trocando do tamanho default pra tabelas no formato utf8mb4 (https://stackoverflow.com/a/31474509/1245235)
    name = models.CharField(u"Nome", max_length=128, null=True, blank=True)
    phone_area = models.PositiveSmallIntegerField(u"DDD", null=True, blank=True)
    phone_number = models.PositiveIntegerField(u"Telefone", null=True, blank=True)

    class Meta:
        ordering = ['name', 'email']
        verbose_name = u"customer"
        verbose_name_plural = u"customers"

    def __unicode__(self):
        return self.email if not self.name else self.name

    def __str__(self):
        return self.__unicode__()


class Operation(Timestamp):
    code = models.CharField(u"Código", max_length=32, unique=True)
    creation_date = models.DateTimeField(u"Gerado em")
    reference = models.CharField(u"Referência", max_length=200, null=True, blank=True)
    redirect_url = models.CharField(u"URL de retorno", max_length=255, null=True, blank=True)
    max_uses = models.SmallIntegerField(u"Máximo de Usos", null=True, blank=True)

    class Meta:
        abstract = True


class Transaction(Timestamp):
    code = models.CharField(u"Código de Transação", max_length=32, unique=True)
    status = models.CharField(u"Situação", max_length=2, choices=PagSeguro.TRANSACTION_STATUS_MAP.items())
    reference = models.CharField(u"Referência", max_length=200, null=True, blank=True)
    gross_value = models.DecimalField(u"Valor Bruto", max_digits=11, decimal_places=2)
    net_value = models.DecimalField(u"Valor Líquido", max_digits=11, decimal_places=2)
    purchase_date = models.DateTimeField(u"Data da Compra")
    last_update_date = models.DateTimeField(u"Última Atualização da Compra")
    customer = models.ForeignKey(Customer, verbose_name=u"Cliente", related_name='%(class)s_transactions', related_query_name='transaction')

    objects = models.Manager()
    paid = TransactionPaidManager()
    pending = TransactionPendingManager()

    class Meta:
        abstract = True

    def save(self, **kwargs):
        u""" Deixamos sempre o código apenas em caracteres alfanuméricos. """
        self.code = Transaction.normalize_transaction_code(self.code)
        super(Transaction, self).save(**kwargs)

    def is_paid(self):
        u""" Verifica se esta transação foi paga. """
        return self.status == PagSeguro.PAGA

    def set_is_paid(self):
        u""" Troca o estado da transação para paga. """
        self.status = PagSeguro.PAGA
        self.save()

    def set_is_canceled(self):
        u""" Troca o estado da transação para cancelada. """
        self.status = PagSeguro.CANCELADA
        self.save()

    def eh_checkout(self):
        from platforms.pagseguro.django.models.checkout import CheckoutTransaction
        return isinstance(self, CheckoutTransaction)

    def eh_pre_approval(self):
        from platforms.pagseguro.django.models.pre_approval import PreApprovalTransaction
        return isinstance(self, PreApprovalTransaction)

    def link_to_operation(self):
        return False

    @staticmethod
    def normalize_transaction_code(code):
        u"""
        Limpa o código da transação para deixar apenas com caracteres alfa-numéricos maiúsculos.

        :param code: código da transação, e.g.: c2efd37f-ab3b-4473-a7d7-44e6c4a6610b
        :type code: str
        :return: código normalizado, e.g.: C2EFD37FAB3B4473A7D744E6C4A6610B
        """
        return code.replace('-', '').upper()

    @staticmethod
    def exists_transaction(code):
        return Transaction.objects.filter(code=Transaction.clean_codigo(code)).exists()

    @classmethod
    @transaction.atomic
    def get_or_create_from_base_transaction(cls, transaction_code, pagseguro_transaction=None):
        u"""
        Direcionamos a criação de uma nova transação corretamente para seu modelo de checkout ou recorrência a partir do código de referência.

        :type transaction_code: str
        :param pagseguro_transaction: se preenchido, não utilizamos a API do PagSeguro para buscar as informações da transação
        :type pagseguro_transaction: pagseguro_v2.parsers.PagSeguroNotificationResponse
        :return: tupla com a instância do modelo e a flag de criação
        :rtype: (platforms.pagseguro.django.base.CheckoutTransaction, bool) | (platforms.pagseguro.django.base.PreApprovalTransaction, bool)
        """
        from platforms.pagseguro.django.models.checkout import Checkout, CheckoutTransaction
        from platforms.pagseguro.django.models.pre_approval import PreApproval, PreApprovalTransaction
        if pagseguro_transaction is None:
            pagseguro_transaction = search_transaction_by_code(transaction_code)
        if Checkout.objects.filter(reference=pagseguro_transaction.reference).exists():
            return CheckoutTransaction.get_or_create_from_base_transaction(transaction_code, pagseguro_transaction)
        elif PreApproval.objects.filter(reference=pagseguro_transaction.reference).exists():
            return PreApprovalTransaction.get_or_create_from_base_transaction(transaction_code, pagseguro_transaction)
        else:
            # TODO: Transação não tem checkout ou recorrência original
            return None, False
