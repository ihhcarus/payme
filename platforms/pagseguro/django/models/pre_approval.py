# -*- coding: utf-8 -*-


from datetime import timedelta, datetime

from django.db import models
from django.db import transaction

from platforms.pagseguro.api.api import search_subscription_by_code, search_transaction_by_code
from platforms.pagseguro.api.models import PagSeguro
from platforms.pagseguro.django.managers import SubscriptionActiveManager, SubscriptionCanceledManager, SubscriptionPendingManager, PaymentOrderManager, SubscriptionManager
from platforms.pagseguro.django.models.base import Operation, Transaction, Customer, Timestamp
from platforms.pagseguro.django.signals import pre_approval_created, pre_approval_transaction_created, pre_approval_transaction_paid, subscription_active, subscription_created, subscription_canceled, pre_approval_transaction_canceled


class PreApproval(Operation):
    PERIODS = (
        ('WEEKLY', u"Semanal"),
        ('MONTHLY', u"Mensal"),
        ('BIMONTHLY', u"Bimestral"),
        ('TRIMONTHLY', u"Trimestral"),
        ('SEMIANNUALLY', u"Semestral"),
        ('YEARLY', u"Anual")
    )
    CHARGE = (
        ('AUTO', u"Automática"),
        ('MANUAL', u"Manual"),
    )

    name = models.CharField(u"Nome", max_length=100)
    amount_per_payment = models.DecimalField(u"Valor por Cobrança", max_digits=6, decimal_places=2)
    details = models.CharField(u"Descrição", max_length=100)
    period = models.CharField(u"Periodicidade", choices=PERIODS, max_length=32)
    charge = models.CharField(u"Cobrança", choices=CHARGE, max_length=32)

    class Meta:
        ordering = ['-created']
        verbose_name = u"pre approval"
        verbose_name_plural = u"pre approvals"

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.reference)

    def __str__(self):
        return self.__unicode__()

    def get_period_days(self):
        u"""
        Mapeia o período da recorrência para o número de dias.
        
        :return: número de dias do período 
        :rtype: int
        """
        return {
            'WEEKLY': 7,
            'MONTHLY': 30,
            'BIMONTHLY': 60,
            'TRIMONTHLY': 90,
            'SEMIANNUALLY': 180,
            'YEARLY': 365,
        }[self.period]

    @classmethod
    def create(cls, operation, pre_approval, redirect_url, max_uses, reference):
        new_pre_approval = cls.objects.create(
            code=operation.code, creation_date=operation.date.replace(tzinfo=None),
            reference=reference, redirect_url=redirect_url, max_uses=max_uses,
            name=pre_approval.name, amount_per_payment=pre_approval.amount_per_payment, details=pre_approval.details, period=pre_approval.period.upper(), charge=pre_approval.charge.upper(),
        )
        pre_approval_created.send(sender=cls, instance=new_pre_approval)
        return new_pre_approval


class PreApprovalTransaction(Transaction):
    pre_approval = models.ForeignKey(PreApproval, verbose_name=u"Recorrência que Gerou", on_delete=models.SET_NULL, null=True, blank=True)
    payment_order = models.ForeignKey('PaymentOrder', verbose_name=u"Ordem de Pagamento", on_delete=models.SET_NULL, null=True, blank=True, related_name='transacoes', related_query_name='transacao')

    class Meta:
        ordering = ['-created']
        verbose_name = u"pre approval transaction"
        verbose_name_plural = u"pre approvals transactions"

    def __unicode__(self):
        return u"%s (%s)" % (self.code, self.reference)

    def __str__(self):
        return self.__unicode__()

    def set_is_paid(self):
        u""" Além de marcar como pago, notifica o signal. """
        super(PreApprovalTransaction, self).set_is_paid()
        pre_approval_transaction_paid.send(sender=self.__class__, instance=self)

    def set_is_canceled(self):
        u""" Além de marcar como cancelada, notifica o signal. """
        super(PreApprovalTransaction, self).set_is_paid()
        pre_approval_transaction_canceled.send(sender=self.__class__, instance=self)

    def link_to_operation(self):
        u""" Tentamos ligar a referência da recorrência que originou esta transação buscando pelo código de referência. """
        try:
            pre_approval = PreApproval.objects.get(reference=self.reference)
            self.pre_approval = pre_approval
            self.save()
            return True
        except PreApproval.DoesNotExist:
            return False

    def set_payment_order(self, payment_order):
        u"""
        Salva a ordem de pagamento relacionada a esta transação.

        :type payment_order: platforms.pagseguro.django.models.pre_approval.PaymentOrder
        """
        self.payment_order = payment_order
        self.save()

    @classmethod
    @transaction.atomic
    def get_or_create_from_base_transaction(cls, transaction_code, pagseguro_transaction=None):
        u"""
        Retorna uma instância do modelo a partir da transação original, indicando se foi criada ou não.

        :type transaction_code: str
        :param pagseguro_transaction: se preenchido, não utilizamos a API do PagSeguro para buscar as informações da transação
        :type pagseguro_transaction: pagseguro_v2.parsers.PagSeguroTransactionSearchResult
        :return: tupla com a instância do modelo e a flag de criação
        :rtype: (platforms.pagseguro.django.base.PreApprovalTransaction, bool)
        """
        try:
            return cls.objects.get(code=Transaction.normalize_transaction_code(transaction_code)), False
        except cls.DoesNotExist:
            if pagseguro_transaction is None:
                pagseguro_transaction = search_transaction_by_code(transaction_code)
            # Alguns códigos são inválidos...? e.g.: subscription 408B5C7BFFFF2ED884A13FA10246D583 e transação C6B6C5CD1E274BB6B543807AA2EE8E25
            if pagseguro_transaction is None:
                return None, False
            customer, _ = Customer.objects.update_or_create(
                email=pagseguro_transaction.sender.get('email'),
                defaults={
                    'name': pagseguro_transaction.sender.get('name'),
                    'phone_area': pagseguro_transaction.sender.get('phone_area'),
                    'phone_number': pagseguro_transaction.sender.get('phone_number'),
                }
            )
            pre_approval_transaction = cls.objects.create(
                code=pagseguro_transaction.code, status=pagseguro_transaction.status, reference=pagseguro_transaction.reference,
                gross_value=pagseguro_transaction.grossAmount, net_value=pagseguro_transaction.netAmount,
                purchase_date=PagSeguro.parse_date(pagseguro_transaction.date),
                last_update_date=PagSeguro.parse_date(pagseguro_transaction.lastEventDate),
                customer=customer
            )
            pre_approval_transaction.link_to_operation()
            pre_approval_transaction_created.send(sender=cls, instance=pre_approval_transaction)
            if pre_approval_transaction.is_paid():
                pre_approval_transaction.set_is_paid()
            return pre_approval_transaction, True


class Subscription(Timestamp):
    code = models.CharField(u"Código", max_length=32, unique=True)
    tracker = models.CharField(u"Identificador", max_length=16)
    name = models.CharField(u"Nome", max_length=128)
    status = models.CharField(u"Situação", max_length=48, choices=PagSeguro.SUBSCRIPTION_STATUS_MAP.items())
    reference = models.CharField(u"Referência", max_length=200, null=True, blank=True)
    subscription_date = models.DateTimeField(u"Inscrito em")
    customer = models.ForeignKey(Customer, verbose_name=u"Cliente", related_name='subscriptions', related_query_name='subscription', null=True, blank=True)
    pre_approval = models.ForeignKey(PreApproval, verbose_name=u"Recorrência que Gerou", on_delete=models.SET_NULL, null=True, blank=True)

    objects = SubscriptionManager()
    active = SubscriptionActiveManager()
    pending = SubscriptionPendingManager()
    canceled = SubscriptionCanceledManager()

    class Meta:
        ordering = ['-created']
        verbose_name = u"subscription"
        verbose_name_plural = u"subscriptions"

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.tracker)

    def __str__(self):
        return self.__unicode__()

    def link_to_operation(self):
        u""" Tentamos ligar a referência da recorrência que originou esta assinatura buscando pelo código de referência. """
        try:
            pre_approval = PreApproval.objects.get(reference=self.reference)
            self.pre_approval = pre_approval
            self.save()
            return True
        except PreApproval.DoesNotExist:
            return False

    def is_active(self):
        return self.status == PagSeguro.ATIVA

    def is_canceled(self):
        u""" Assinatura não é válida se não está em alguns estados permitidos. """
        return self.status not in PagSeguro.NOT_CANCELED_STATUSES

    def set_is_active(self):
        self.status = PagSeguro.ATIVA
        self.save()
        subscription_active.send(sender=self.__class__, instance=self)

    def set_is_canceled(self, status):
        self.status = status
        self.save()
        subscription_canceled.send(sender=self.__class__, instance=self)

    def is_expired(self, default_period):
        u"""
        Verifica se a assinatura já expirou pelo estado de ativa e data de expiração.
        Se não possui ordens de pagamento, retorna que não está expirada
        
        :param default_period: quantidade de dias padrão para usar caso período não esteja disponível
        :type default_period: int
        :return: se a assinatura já expirou
        :rtype: bool
        """
        if self.is_active():
            return False
        paid_payment_order = self.payment_orders.pagas()
        if paid_payment_order.exists():
            latest_payment_order = paid_payment_order.first()
            expiry_date = latest_payment_order.get_expiry_date(default_period)
            return datetime.now() > expiry_date
        return False

    def get_period_days(self):
        u"""
        Buscamos a periodicidade da assinatura na recorrência que gerou ela.
        
        :return: periodicidade, se a recorrência está disponível 
        :rtype: int | None
        """
        if self.pre_approval:
            return self.pre_approval.get_period_days()
        else:
            return None

    def get_expiry_date(self):
        u"""
        Fornece a data de expiração da assinatura a partir da última ordem de pagamento paga.
        
        :return: data de expiração, se disponível
        :rtype: datetime | None
        """
        paid_payment_order = self.payment_orders.pagas()
        if paid_payment_order.exists():
            return paid_payment_order.first().get_expiry_date()
        return None

    @classmethod
    @transaction.atomic
    def get_or_create_from_base_subscription(cls, subscription_code, pagseguro_subscription=None):
        u"""
        Retorna uma instância do modelo a partir da inscrição original, indicando se foi criada ou não.

        :type subscription_code: str
        :param pagseguro_subscription: se preenchido, não utilizamos a API do PagSeguro para buscar as informações da transação
        :type pagseguro_subscription: pagseguro_v2.parsers.PagSeguroPreApprovalSearch
        :return: tupla com a instância do modelo e a flag de criação
        :rtype: (platforms.pagseguro.django.base.PreApprovalTransaction, bool)
        """
        try:
            return cls.objects.get(code=subscription_code), False
        except cls.DoesNotExist:
            if pagseguro_subscription is None:
                pagseguro_subscription = search_subscription_by_code(subscription_code)
            customer_email = pagseguro_subscription.sender.get('email')
            customer = None
            if customer_email:
                customer, _ = Customer.objects.update_or_create(
                    email=customer_email,
                    defaults={
                        'name': pagseguro_subscription.sender.get('name'),
                        'phone_area': pagseguro_subscription.sender.get('phone_area'),
                        'phone_number': pagseguro_subscription.sender.get('phone_number'),
                    }
                )
            subscription = cls.objects.create(
                code=pagseguro_subscription.code, tracker=pagseguro_subscription.tracker, name=pagseguro_subscription.name,
                status=pagseguro_subscription.status, reference=pagseguro_subscription.reference,
                subscription_date=PagSeguro.parse_date(pagseguro_subscription.date),
                customer=customer
            )
            subscription.link_to_operation()
            subscription_created.send(sender=cls, instance=subscription)
            if subscription.is_active():
                subscription.set_is_active()
            if subscription.is_canceled():
                subscription.set_is_canceled(pagseguro_subscription.status)
            return subscription, True


class PaymentOrder(Timestamp):
    code = models.CharField(u"Código", max_length=32, unique=True)
    status = models.CharField(u"Situação", max_length=48, choices=PagSeguro.PAYMENT_ORDER_STATUS_MAP.items())
    amount = models.DecimalField(u"Valor", max_digits=6, decimal_places=2)
    last_update_date = models.DateTimeField(u"Última Atualização")
    scheduled_date = models.DateTimeField(u"Data Programada")
    subscription = models.ForeignKey(Subscription, verbose_name=u"Assinatura", related_name='payment_orders', related_query_name='payment_order', on_delete=models.CASCADE)

    objects = PaymentOrderManager()

    class Meta:
        ordering = ['-created']
        verbose_name = u"payment order"
        verbose_name_plural = u"payment orders"

    def __unicode__(self):
        return u"%s" % self.code

    def __str__(self):
        return self.__unicode__()

    def get_expiry_date(self, default_period=None):
        u"""
        Buscamos a data que a validade deste pagamento expira.  
        
        :param default_period: quantidade de dias padrão para usar caso período não esteja disponível
        :type default_period: int
        :return: data de expiração, se disponível
        :rtype: datetime | None
        """
        period = self.subscription.get_period_days()
        if not period:
            period = default_period
        if period:
            return self.scheduled_date + timedelta(days=period)
        else:
            return None

    @classmethod
    @transaction.atomic
    def get_or_create_from_base_payment_order(cls, subscription_code, payment_order_code, pagseguro_payment_order):
        u"""
        Cria ou busca uma nova ordem de pagamento a partir do objeto do PagSeguro.

        :param subscription_code: código da assinatura que gerou a ordem de pagamento
        :type subscription_code: str
        :param payment_order_code: código da ordem de pagamento
        :type payment_order_code: str
        :param pagseguro_payment_order: objeto do PagSeguro com os campos da ordem de pagamento
        :type pagseguro_payment_order: dict
        :return: tupla com a instância da ordem de pagamento e uma flag indicando se for criada
        :rtype: (platforms.pagseguro.django.models.pre_approval.PaymentOrder, bool)
        """
        try:
            return cls.objects.get(code=payment_order_code), False
        except cls.DoesNotExist:
            related_transactions = pagseguro_payment_order.get('transactions', [])
            related_transactions_instances = []
            # Pode vir uma ou várias transações relacionadas
            if isinstance(related_transactions, dict):
                pre_approval_transaction, _ = PreApprovalTransaction.get_or_create_from_base_transaction(related_transactions.get('code'))
                if pre_approval_transaction:
                    related_transactions_instances.append(pre_approval_transaction)
            else:
                for related_transaction in related_transactions:
                    pre_approval_transaction, _ = PreApprovalTransaction.get_or_create_from_base_transaction(related_transaction.get('code'))
                    if pre_approval_transaction:
                        related_transactions_instances.append(pre_approval_transaction)
            payment_order = cls.objects.create(
                code=payment_order_code, status=pagseguro_payment_order.get('status'), amount=pagseguro_payment_order.get('amount'),
                last_update_date=PagSeguro.parse_date(pagseguro_payment_order.get('lastEventDate')),
                scheduled_date=PagSeguro.parse_date(pagseguro_payment_order.get('schedulingDate')),
                subscription=Subscription.get_or_create_from_base_subscription(subscription_code)[0]
            )
            for related_transactions_instance in related_transactions_instances:
                related_transactions_instance.set_payment_order(payment_order)
            return payment_order, True

    @classmethod
    @transaction.atomic
    def update_from_base_payment_order(cls, payment_order_code, pagseguro_payment_order):
        u"""
        Atualiza o status da ordem de pagamento e suas transações a partir do objeto do PagSeguro.

        :param payment_order_code: código da ordem de pagamento
        :type payment_order_code: str
        :param pagseguro_payment_order: objeto do PagSeguro com os campos da ordem de pagamento
        :type pagseguro_payment_order: dict
        :return: tupla com a instância da ordem de pagamento e uma flag indicando se for criada
        :rtype: (platforms.pagseguro.django.models.pre_approval.PaymentOrder, bool)
        """
        try:
            instance = cls.objects.get(code=payment_order_code)
            instance.status = pagseguro_payment_order.get('status')
            instance.save()
            related_transactions = pagseguro_payment_order.get('transactions', [])
            # Pode vir uma ou várias transações relacionadas
            if isinstance(related_transactions, dict):
                pre_approval_transaction, _ = PreApprovalTransaction.get_or_create_from_base_transaction(related_transactions.get('code'))
                if pre_approval_transaction:
                    instance.transacoes.filter(code=pre_approval_transaction.code).update(status=related_transactions.get('status'))
            else:
                for related_transaction in related_transactions:
                    pre_approval_transaction, _ = PreApprovalTransaction.get_or_create_from_base_transaction(related_transaction.get('code'))
                    if pre_approval_transaction:
                        instance.transacoes.filter(code=pre_approval_transaction.code).update(status=related_transaction.get('status'))
            return instance, True
        except cls.DoesNotExist:
            return None, False
