# -*- coding: utf-8 -*-


from django.db import models
from django.db import transaction

from platforms.pagseguro.api.api import search_transaction_by_code
from platforms.pagseguro.api.models import PagSeguro
from platforms.pagseguro.api.models import Shipping as ShippingAPI
from platforms.pagseguro.django.models.base import Operation, Timestamp, Transaction, Customer
from platforms.pagseguro.django.signals import checkout_transaction_created, checkout_created, checkout_transaction_paid, checkout_transaction_canceled


class Checkout(Operation):
    max_age = models.IntegerField(u"Validade (segs.)", null=True, blank=True)
    total_value = models.DecimalField(u"Valor Total", max_digits=11, decimal_places=2)

    class Meta:
        ordering = ['-created']
        verbose_name = u"checkout"
        verbose_name_plural = u"checkouts"

    def __unicode__(self):
        return u"%s" % self.code

    def __str__(self):
        return self.__unicode__()

    @classmethod
    @transaction.atomic
    def create(cls, operation, items, shipping, redirect_url, max_uses, reference, max_age):
        new_checkout = cls.objects.create(
            code=operation.code, creation_date=operation.date.replace(tzinfo=None),
            reference=reference, redirect_url=redirect_url, max_uses=max_uses,
            total_value=0.00, max_age=max_age
        )
        total_value = 0.00
        for item in items:
            CheckoutItem.objects.create(
                identifier=item.id,
                description=item.description,
                amount=item.amount,
                quantity=item.quantity,
                weight=item.weight,
                checkout=new_checkout
            )
            total_value += float(item.amount)
        # O checkout é criado sem preço e preenchido após criar os itens e somar os valores
        new_checkout.total_value = total_value
        new_checkout.save()
        if shipping is not None:
            Shipping.objects.create(
                type=shipping.type,
                postal_code=shipping.postal_code,
                street=shipping.street,
                number=shipping.number,
                complement=shipping.complement,
                district=shipping.district,
                city=shipping.city,
                state=shipping.state,
                checkout=new_checkout,
            )
        checkout_created.send(sender=cls, instance=new_checkout)
        return new_checkout


class CheckoutItem(Timestamp):
    identifier = models.CharField(u"Identificador", max_length=100)
    description = models.CharField(u"Descrição", max_length=100)
    amount = models.DecimalField(u"Valor", max_digits=9, decimal_places=2)
    quantity = models.PositiveSmallIntegerField(u"Quantidade")
    weight = models.PositiveSmallIntegerField(u"Peso", null=True, blank=True)
    checkout = models.ForeignKey(Checkout, verbose_name=u"Checkout", related_name='items', related_query_name='item', on_delete=models.CASCADE)

    class Meta:
        ordering = ['-created']
        verbose_name = u"checkout item"
        verbose_name_plural = u"checkouts items"

    def __unicode__(self):
        return u"%s" % self.identifier

    def __str__(self):
        return self.__unicode__()


class Shipping(Timestamp):
    SHIPPING_TYPES = (
        (ShippingAPI.TYPE_PAC, u"PAC"),
        (ShippingAPI.TYPE_SEDEX, u"SEDEX")
    )

    type = models.IntegerField(choices=SHIPPING_TYPES)
    postal_code = models.CharField(max_length=8)
    street = models.CharField(max_length=256, null=True, blank=True)
    number = models.CharField(max_length=5, null=True, blank=True)
    complement = models.CharField(max_length=128, null=True, blank=True)
    district = models.CharField(max_length=256, null=True, blank=True)
    city = models.CharField(max_length=256, null=True, blank=True)
    state = models.CharField(max_length=2, null=True, blank=True)
    checkout = models.ForeignKey(Checkout, verbose_name=u"Checkout", related_name='shippings', related_query_name='shipping', on_delete=models.CASCADE)


class CheckoutTransaction(Transaction):
    checkout = models.ForeignKey(Checkout, verbose_name=u"Checkout que Gerou", on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        ordering = ['-created']
        verbose_name = u"checkout transaction"
        verbose_name_plural = u"checkouts transactions"

    def __unicode__(self):
        return u"%s (%s)" % (self.code, self.reference)

    def __str__(self):
        return self.__unicode__()

    def set_is_paid(self):
        u""" Além de marcar como pago, notifica o signal. """
        super(CheckoutTransaction, self).set_is_paid()
        checkout_transaction_paid.send(sender=self.__class__, instance=self)

    def set_is_canceled(self):
        u""" Além de marcar como cancelada, notifica o signal. """
        super(CheckoutTransaction, self).set_is_paid()
        checkout_transaction_canceled.send(sender=self.__class__, instance=self)

    def link_to_operation(self):
        u""" Tentamos ligar a referência o checkout que originou esta transação buscando pelo código de referência. """
        try:
            checkout = Checkout.objects.get(reference=self.reference)
            self.checkout = checkout
            self.save()
            return True
        except Checkout.DoesNotExist:
            return False

    @classmethod
    @transaction.atomic
    def get_or_create_from_base_transaction(cls, transaction_code, pagseguro_transaction=None):
        u"""
        Retorna uma instância do modelo a partir da transação original, indicando se foi criada ou não.

        :type transaction_code: str
        :param pagseguro_transaction: se preenchido, não utilizamos a API do PagSeguro para buscar as informações da transação
        :type pagseguro_transaction: pagseguro_v2.parsers.PagSeguroTransactionSearchResult
        :return: tupla com a instância do modelo e a flag de criação
        :rtype: (platforms.pagseguro.django.base.CheckoutTransaction, bool)
        """
        try:
            return cls.objects.get(code=Transaction.normalize_transaction_code(transaction_code)), False
        except cls.DoesNotExist:
            if pagseguro_transaction is None:
                pagseguro_transaction = search_transaction_by_code(transaction_code)
            customer, _ = Customer.objects.update_or_create(
                email=pagseguro_transaction.sender.get('email'),
                defaults={
                    'name': pagseguro_transaction.sender.get('name'),
                    'phone_area': pagseguro_transaction.sender.get('phone_area'),
                    'phone_number': pagseguro_transaction.sender.get('phone_number'),
                }
            )
            checkout_transaction = cls.objects.create(
                code=pagseguro_transaction.code, status=pagseguro_transaction.status, reference=pagseguro_transaction.reference,
                gross_value=pagseguro_transaction.grossAmount, net_value=pagseguro_transaction.netAmount,
                purchase_date=PagSeguro.parse_date(pagseguro_transaction.date),
                last_update_date=PagSeguro.parse_date(pagseguro_transaction.lastEventDate),
                customer=customer
            )
            checkout_transaction.link_to_operation()
            checkout_transaction_created.send(sender=cls, instance=checkout_transaction)
            if checkout_transaction.is_paid():
                checkout_transaction.set_is_paid()
            return checkout_transaction, True
