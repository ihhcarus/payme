# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-28 11:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pagseguro_django', '0002_subscription'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='pre_approval',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='pagseguro_django.PreApproval', verbose_name='Recorr\xeancia que Gerou'),
        ),
    ]
