# -*- coding: utf-8 -*-


from django.db import models

from platforms.pagseguro.api.models import PagSeguro


class TransactionPaidManager(models.Manager):
    u""" Filtra pelas transações pagas. """

    def get_queryset(self):
        return super(TransactionPaidManager, self).get_queryset().filter(status=PagSeguro.PAGA)


class TransactionPendingManager(models.Manager):
    u""" Filtra pelas transações pendentes. """

    def get_queryset(self):
        return super(TransactionPendingManager, self).get_queryset().exclude(status=PagSeguro.PAGA)


class SubscriptionManager(models.Manager):
    u""" Manager padrão para assinaturas. Fornece funções para facilitar filtro. """

    def ativas(self):
        u""" Filtra pelas assinaturas ativas. """
        return self.filter(status=PagSeguro.ATIVA).order_by('-subscription_date')

    def canceladas(self):
        u""" Filtra pelas assinaturas que já foram ativas e não são mais. """
        return self.filter(status__in=PagSeguro.CANCELED_STATUSES).order_by('-subscription_date')


class SubscriptionActiveManager(models.Manager):
    u""" Filtra pelas assinaturas ativas. """

    def get_queryset(self):
        return super(SubscriptionActiveManager, self).get_queryset().filter(status=PagSeguro.ATIVA)


class SubscriptionPendingManager(models.Manager):
    u""" Filtra pelas assinaturas pendentes. """

    def get_queryset(self):
        return super(SubscriptionPendingManager, self).get_queryset().filter(status__in=PagSeguro.NOT_ACTIVE_STATUSES)


class SubscriptionCanceledManager(models.Manager):
    u""" Filtra pelas assinaturas canceladas. """

    def get_queryset(self):
        return super(SubscriptionCanceledManager, self).get_queryset().exclude(status__in=PagSeguro.NOT_CANCELED_STATUSES)


class PaymentOrderManager(models.Manager):
    u""" Fornece filtros para as ordens de pagamento. """

    def pagas(self):
        u""" Filtra pelas ordens pagas. """
        return self.filter(status=PagSeguro.ORDEM_PAGA).order_by('-scheduled_date')

    def nao_pagas(self):
        u""" Filtra pelas ordens não pagas. """
        return self.filter(status=PagSeguro.NAO_PAGA).order_by('scheduled_date')
