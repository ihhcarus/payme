# -*- coding: utf-8 -*-


from __future__ import absolute_import  # Deixa esse cara aqui pra não zicar o import de módulos com mesmo nome!

from django.apps import AppConfig


class PagSeguroDjangoConfig(AppConfig):
    name = 'platforms.pagseguro.django'
    label = 'pagseguro_django'
    verbose_name = u"PagSeguro Django"
