# -*- coding: utf-8 -*-


from django.contrib import admin

from platforms.pagseguro.django.models.base import Customer


class ReadOnlyTabularInline(admin.TabularInline):
    u""" Só mostra as informações relacionadas, sem poder alterar. """

    extra = 0
    can_delete = False
    show_change_link = True

    def has_add_permission(self, request):
        u""" Hack pra tirar o botão de adicionar um novo inline. """
        return False


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'created']
    search_fields = ['name', 'email']

    def get_inline_instances(self, request, obj=None):
        from platforms.pagseguro.django.admin.checkout import CheckoutTransactionInline
        return [CheckoutTransactionInline]


class OperationAdmin(admin.ModelAdmin):
    list_display = ['code', 'creation_date', 'reference_part', 'max_uses']
    search_fields = ['code', 'reference']

    def reference_part(self, instance):
        return instance.reference[0:32] + ('...' if len(instance.reference) > 32 else '')

    reference_part.short_description = u"Referência"
