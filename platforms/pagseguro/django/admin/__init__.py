# -*- coding: utf-8 -*-


from platforms.pagseguro.django.admin.base import *
from platforms.pagseguro.django.admin.checkout import *
from platforms.pagseguro.django.admin.pre_approval import *
