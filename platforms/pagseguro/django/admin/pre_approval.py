# -*- coding: utf-8 -*-


from django.contrib import admin

from platforms.pagseguro.django.admin.base import ReadOnlyTabularInline, OperationAdmin
from platforms.pagseguro.django.api import get_subscriptions_payment_orders
from platforms.pagseguro.django.models.pre_approval import PreApprovalTransaction, PreApproval, Subscription, PaymentOrder
from platforms.pagseguro.django.signals import subscription_created, pre_approval_transaction_created


@admin.register(PreApproval)
class PreApprovalAdmin(OperationAdmin):
    def get_list_display(self, request):
        return super(PreApprovalAdmin, self).get_list_display(request) + ['name', 'amount_per_payment', 'period']


class PreApprovalTransactionInline(ReadOnlyTabularInline):
    model = PreApprovalTransaction
    fields = ['code', 'status', 'purchase_date', 'gross_value', 'net_value']
    readonly_fields = ['code', 'status', 'purchase_date', 'gross_value', 'net_value']


@admin.register(PreApprovalTransaction)
class PreApprovalTransactionAdmin(admin.ModelAdmin):
    list_display = ['code', 'customer', 'status', 'purchase_date', 'gross_value', 'net_value', 'reference']
    list_filter = ['status']
    search_fields = ['reference']
    actions = ['link_to_operation_action', 'send_pre_approval_created_signal_action', 'send_pre_approval_paid_signal_action']

    def link_to_operation_action(self, request, queryset):
        u"""
        Tentamos ligar a referência da recorrência que originou esta transação buscando pelo código de referência.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.PreApprovalTransaction]
        """
        success = 0
        for pre_approval in queryset.iterator():
            if pre_approval.link_to_operation():
                success += 1
        self.message_user(request, u"Linked %d pre approvals transactions successfully." % success)

    link_to_operation_action.short_description = u"Link pre approvals transactions to pre approvals"

    def send_pre_approval_created_signal_action(self, request, queryset):
        u"""
        Enviamos um sinal de "criação" para as recorrências.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.PreApprovalTransaction]
        """
        for pre_approval in queryset.iterator():
            pre_approval_transaction_created.send(pre_approval.__class__, instance=pre_approval)
        self.message_user(request, u"Sent %d pre approval created signal successfully" % queryset.count())

    send_pre_approval_created_signal_action.short_description = u"Send pre approval created signal"

    def send_pre_approval_paid_signal_action(self, request, queryset):
        u"""
        Enviamos um sinal de "paga" para as recorrências pagas.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.PreApprovalTransaction]
        """
        count = 0
        for pre_approval in queryset.iterator():
            if pre_approval.is_paid():
                pre_approval.set_is_paid()
                count += 1
        self.message_user(request, u"Sent %d pre approval paid signal successfully" % count)

    send_pre_approval_paid_signal_action.short_description = u"Send pre approval paid signal"


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ['code', 'tracker', 'name', 'status', 'reference', 'subscription_date', 'customer']
    list_filter = ['status']
    search_fields = ['reference']
    actions = [
        'send_subscription_created_signal_action', 'send_subscription_active_signal_action', 'send_subscription_canceled_signal_action',
        'get_subscriptions_payment_orders_action', 'link_to_operation_action'
    ]

    def send_subscription_created_signal_action(self, request, queryset):
        u"""
        Enviamos um sinal de "criação" para as assinaturas.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.Subscription]
        """
        for subscription in queryset.iterator():
            subscription_created.send(subscription.__class__, instance=subscription)
        self.message_user(request, u"Sent %d subscription created signal successfully" % queryset.count())

    send_subscription_created_signal_action.short_description = u"Send subscription created signal"

    def send_subscription_active_signal_action(self, request, queryset):
        u"""
        Enviamos um sinal de "ativa" para as assinaturas ativas.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.Subscription]
        """
        count = 0
        for subscription in queryset.iterator():
            if subscription.is_active():
                subscription.set_is_active()
                count += 1
        self.message_user(request, u"Sent %d subscription active signal successfully" % count)

    send_subscription_active_signal_action.short_description = u"Send subscription active signal"

    def send_subscription_canceled_signal_action(self, request, queryset):
        u"""
        Enviamos um sinal de "cancelada" para as assinaturas canceladas.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.Subscription]
        """
        count = 0
        for subscription in queryset.iterator():
            if subscription.is_canceled():
                subscription.set_is_canceled(subscription.status)
                count += 1
        self.message_user(request, u"Sent %d subscription canceled signal successfully" % count)

    send_subscription_canceled_signal_action.short_description = u"Send subscription canceled signal"

    def link_to_operation_action(self, request, queryset):
        u"""
        Tentamos ligar a referência da recorrência que originou esta assinatura buscando pelo código de referência.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.Subscription]
        """
        success = 0
        for subscription in queryset.iterator():
            if subscription.link_to_operation():
                success += 1
        self.message_user(request, u"Linked %d subscriptions successfully." % success)

    link_to_operation_action.short_description = u"Link subscriptions to pre approvals"

    def get_subscriptions_payment_orders_action(self, request, queryset):
        u"""
        Buscamos as ordens de pagamento das assinaturas.

        :type queryset: django.db.models.query.QuerySet[platforms.pagseguro.django.models.pre_approval.Subscription]
        """
        success = 0
        for subscription in queryset.iterator():
            result = get_subscriptions_payment_orders(subscription.code)
            if result:
                success += 1
        self.message_user(request, u"Processed payment orders for %d subscriptions successfully." % success)

    get_subscriptions_payment_orders_action.short_description = u"Process subscriptions payment orders"


@admin.register(PaymentOrder)
class PaymentOrderAdmin(admin.ModelAdmin):
    list_display = ['code', 'status', 'amount', 'scheduled_date', 'subscription']
    list_filter = ['status']
