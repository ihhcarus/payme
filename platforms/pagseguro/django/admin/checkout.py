# -*- coding: utf-8 -*-


from django.contrib import admin

from platforms.pagseguro.django.admin.base import ReadOnlyTabularInline, OperationAdmin
from platforms.pagseguro.django.models.checkout import CheckoutTransaction, Checkout, CheckoutItem, Shipping


class CheckoutItemInline(ReadOnlyTabularInline):
    model = CheckoutItem
    fields = ['identifier', 'description', 'amount', 'quantity']
    readonly_fields = ['identifier', 'description', 'amount', 'quantity']


@admin.register(CheckoutItem)
class CheckoutItemAdmin(admin.ModelAdmin):
    list_display = ['identifier', 'description', 'amount', 'quantity']
    search_fields = ['identifier', 'description']


class ShippingInline(ReadOnlyTabularInline):
    model = Shipping
    fields = ['postal_code', 'street', 'city', 'state']
    readonly_fields = ['postal_code', 'street', 'city', 'state']


@admin.register(Checkout)
class CheckoutAdmin(OperationAdmin):
    inlines = [CheckoutItemInline, ShippingInline]

    def get_list_display(self, request):
        return super(CheckoutAdmin, self).get_list_display(request) + ['max_age', 'total_value']


class CheckoutTransactionInline(ReadOnlyTabularInline):
    model = CheckoutTransaction
    fields = ['code', 'status', 'purchase_date', 'gross_value', 'net_value']
    readonly_fields = ['code', 'status', 'purchase_date', 'gross_value', 'net_value']


@admin.register(CheckoutTransaction)
class CheckoutTransactionAdmin(admin.ModelAdmin):
    list_display = ['code', 'customer', 'status', 'purchase_date', 'gross_value', 'net_value', 'reference']
    list_filter = ['status']
    search_fields = ['reference']


@admin.register(Shipping)
class ShippingAdmin(admin.ModelAdmin):
    list_display = ['checkout', 'postal_code', 'street', 'city', 'state']
    list_filter = ['state']
    search_fields = ['checkout__code', 'checkout__reference']
