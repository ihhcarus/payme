# -*- coding: utf-8 -*-


from django import dispatch


# region Checkout

checkout_created = dispatch.Signal(providing_args=['instance'])
u""" Indica quando um novo checkout foi adicionado pela função `platforms.pagseguro.django.models.checkout.Checkout#create`. """

checkout_transaction_created = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma nova transação de checkout foi adicionada pela função `platforms.pagseguro.django.models.checkout.CheckoutTransaction#get_or_create_from_base_transaction`. """

checkout_transaction_paid = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma transação foi marcada como paga pela função `platforms.pagseguro.django.models.checkout.CheckoutTransaction#set_is_paid`. """

checkout_transaction_canceled = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma transação foi marcada como cancelada pela função `platforms.pagseguro.django.models.checkout.CheckoutTransaction#set_is_canceled`. """
# endregion


# region Pre Approval

pre_approval_created = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma nova recorrência foi adicionada pela função `platforms.pagseguro.django.models.pre_approval.PreApproval#create`. """

pre_approval_transaction_created = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma nova transação de recorrência foi adicionada pela função `platforms.pagseguro.django.models.pre_approval.PreApprovalTransaction#get_or_create_from_base_transaction`. """

pre_approval_transaction_paid = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma transação foi marcada como paga pela função `platforms.pagseguro.django.models.pre_approval.PreApprovalTransaction#set_is_paid`. """

pre_approval_transaction_canceled = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma transação foi marcada como cancelada pela função `platforms.pagseguro.django.models.pre_approval.PreApprovalTransaction#set_is_canceled`. """
# endregion


# region Subscription

subscription_created = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma assinatura foi marcada como ativa pela função `platforms.pagseguro.django.models.pre_approval.Subscription#get_or_create_from_base_subscription`. """

subscription_active = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma assinatura foi marcada como ativa pela função `platforms.pagseguro.django.models.pre_approval.Subscription#set_is_active`. """

subscription_canceled = dispatch.Signal(providing_args=['instance'])
u""" Indica quando uma assinatura foi marcada como inativa pela função `platforms.pagseguro.django.models.pre_approval.Subscription#set_is_active`. """
# endregion
