# -*- coding: utf-8 -*-


from django.conf import settings
from pagseguro_v2 import PagSeguro


def create_checkout(items, shipping=None, redirect_url=None, max_uses=None, max_age=None, reference_prefix=None, reference=None):
    u"""
    Cria um pagamento de um produto que pode ser comprado por um usuário.

    :param shipping: informações de entrega
    :type shipping: platforms.pagseguro.api.models.Shipping
    :param items: informações sobre os itens do pagamento que será criado
    :type items: list[platforms.pagseguro.api.models.CheckoutItem] | platforms.pagseguro.api.models.CheckoutItem
    :param redirect_url: URL que o PagSeguro vai redirecionar após o pagamento
    :type redirect_url: str
    :param max_uses: número máximo de utilizações deste checkout
    :type max_uses: int
    :param max_age: tempo máximo (em segundos) que este checkout fica disponível
    :type max_age: int
    :param reference_prefix: prefixo do código de referência do pagamento, e.g., 'LIV' que forma 'LIV01', 'LIV02'
    :type reference_prefix:
    :param reference: código de referência do pagamento, e.g., 'LOJA' que forma 'LIVLOJA' ou 'BLOG' que forma 'LIVBLOG'
    :type reference: str
    :return: objeto com o resultado da requisição de pagamento
    :rtype: pagseguro_v2.parsers.PagSeguroCheckoutResponse
    """
    if not reference:
        reference = 'PAGAMENTO'
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    if reference_prefix:
        config.update({'REFERENCE_PREFIX': reference_prefix})
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    pg.reference = reference
    pg.redirect_url = redirect_url
    pg.max_age = max_age
    pg.max_uses = max_uses
    if not isinstance(items, list):
        items = [items]
    for item in items:
        pg.add_item(**vars(item))
    if shipping is not None:
        pg.shipping = vars(shipping)
    response = pg.checkout()
    if response.errors:
        raise ValueError(response.errors)
    return response


def create_pre_approval(pre_approval, redirect_url=None, max_uses=None, reference_prefix=None, reference=None):
    u"""
    Cria uma recorrência que poderá ser assinada por um usuário.

    :param redirect_url: URL que o PagSeguro vai redirecionar após o pagamento
    :type redirect_url: str
    :param max_uses: número máximo de utilizações deste checkout
    :type max_uses: int
    :param pre_approval: informações sobre a recorrência que será criada
    :type pre_approval: platforms.pagseguro.api.models.PreApproval
    :param reference_prefix: prefixo do código de referência da recorrência, e.g., 'LIV' que forma 'LIV01', 'LIV02'
    :type reference_prefix:
    :param reference: código de referência da recorrência, e.g., 'LOJA' que forma 'LIVLOJA' ou 'BLOG' que forma 'LIVBLOG'
    :type reference: str
    :return: objeto com o resultado da requisição de recorrência
    :rtype: pagseguro_v2.parsers.PagSeguroPreApprovalPayment
    """
    if not reference:
        reference = 'RECORRENCIA'
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    if reference_prefix:
        config.update({'REFERENCE_PREFIX': reference_prefix})
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    pg.reference = reference
    pg.redirect_url = redirect_url
    pg.max_uses = max_uses
    pg.pre_approval = vars(pre_approval)
    response = pg.pre_approval_ask_payment()
    if response.errors:
        raise ValueError(response.errors)
    return response


def search_transaction_by_code(code):
    u"""
    Busca as informações de uma transações pelo seu código.

    :param code: código da transação
    :type code: str
    :return: informações da transação, se encontrada
    :rtype: pagseguro_v2.parsers.PagSeguroNotificationResponse | None
    """
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    response = pg.check_transaction(code)
    if response.errors or not hasattr(response, 'code'):  # Erro ou transação não encontrada (sem o atributo de código)
        return None
    itens = response.items.get('item', [])
    # Quando tem apenas um item retorna o dicionário do mesmo diretamente ao invés da lista de dicionários
    if itens and not isinstance(itens, list):
        response.items['item'] = [itens]
    return response


def search_transactions_by_reference(reference):
    u"""
    Busca as informações de transações a partir do código de referência.

    :param reference: código da referência completo (prefixo e corpo) da transação
    :type reference: str
    :return: lista de transações
    :rtype: list[pagseguro_v2.parsers.PagSeguroTransactionSearchResult]
    """
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    return pg.query_transactions_by_reference(reference)


def search_transactions_by_date_range(initial_date, final_date):
    u"""
    Busca as informações de transações dentro de uma data inicial e final.

    :param initial_date: data de início da busca (até 6 meses atrás)
    :type initial_date: datetime.datetime
    :param final_date: data de fim da busca (até 30 dias da data inicial)
    :type final_date: datetime.datetime
    :return: lista de transações
    :rtype: list[pagseguro_v2.parsers.PagSeguroTransactionSearchResult]
    """
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    return pg.query_transactions(initial_date, final_date)


def search_subscription_by_code(code):
    u"""
    Busca as informações de uma assinatura pelo seu código.

    :param code: código da assinatura
    :type code: str
    :return: informações da assinatura, se encontrada
    :rtype: pagseguro_v2.parsers.PagSeguroPreApproval | None
    """
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    response = pg.check_pre_approval(code)
    if response.errors or not getattr(response, 'code'):  # Erro ou transação não encontrada (sem valor no atributo de código)
        return None
    return response


def search_subscriptions_by_date_range(initial_date, final_date):
    u"""
    Busca as informações de assinaturas dentro de uma data inicial e final.

    :param final_date: data de fim da busca (até 30 dias da data inicial)
    :type final_date: datetime.datetime
    :param initial_date: data de início da busca (até 6 meses atrás)
    :type initial_date: datetime.datetime
    :return: lista de pagamentos
    :rtype: list[pagseguro_v2.parsers.PagSeguroPreApprovalSearch]
    """
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    return pg.query_pre_approvals(initial_date, final_date)


def get_subscriptions_payment_orders(code):
    u"""
    Busca as ordens de pagamento de uma assinatura.

    :param code: código da assinatura
    :type code: str
    :return: lista de ordens de pagamento
    :rtype: list[dict]
    """
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    return pg.query_pre_approvals_payment_orders(code)


def cancel_subscription(code):
    u"""
    Cancela uma assinatura.

    :param code: código da assinatura
    :type code: str
    :return: se o cancelamento foi um sucesso ou não
    :rtype: pagseguro_v2.parsers.PagSeguroPreApprovalCancel
    """
    config = {'sandbox': getattr(settings, 'PAGSEGURO_IS_SANDBOX', True)}
    pg = PagSeguro(settings.PAGSEGURO_API_EMAIL, settings.PAGSEGURO_API_TOKEN, config=config)
    response = pg.pre_approval_cancel(code)
    return response
