# -*- coding: utf-8 -*-


from datetime import datetime


class PagSeguro(object):
    u""" Instância com constantes de mapeamento da API do PagSeguro. """

    AGUARDANDO_PAGAMENTO = '1'
    EM_ANALISE = '2'
    PAGA = '3'
    DISPONIVEL = '4'
    EM_DISPUTA = '5'
    DEVOLVIDA = '6'
    CANCELADA = '7'
    DEBITADO = '8'
    RETENCAO_TEMPORARIA = '9'

    TRANSACTION_STATUS_MAP = {
        AGUARDANDO_PAGAMENTO: u"Aguardando pagamento",
        EM_ANALISE: u"Em análise",
        PAGA: u"Paga",
        DISPONIVEL: u"Disponível",
        EM_DISPUTA: u"Em disputa",
        DEVOLVIDA: u"Devolvida",
        CANCELADA: u"Cancelada",
        DEBITADO: u"Debitado",
        RETENCAO_TEMPORARIA: u"Retenção temporária"
    }

    ATIVA = 'ACTIVE'
    INICIADA, PENDENTE, TROCANDO_PAGAMENTO = 'INITIATED', 'PENDING', 'PAYMENT_METHOD_CHANGE'
    SUSPENDED, CANCELLED, CANCELLED_BY_RECEIVER, CANCELLED_BY_SENDER, EXPIRED = 'SUSPENDED', 'CANCELLED', 'CANCELLED_BY_RECEIVER', 'CANCELLED_BY_SENDER', 'EXPIRED'

    NOT_CANCELED_STATUSES = [ATIVA, INICIADA, PENDENTE, TROCANDO_PAGAMENTO]
    u""" Status de cancelamento após estar ativa. """

    NOT_ACTIVE_STATUSES = [INICIADA, PENDENTE, TROCANDO_PAGAMENTO]
    u""" Status antes do pagamento estar ativa. """

    CANCELED_STATUSES = [SUSPENDED, CANCELLED, CANCELLED_BY_RECEIVER, CANCELLED_BY_SENDER, EXPIRED]
    u""" Status após estar ativa e que não são mais. """

    SUBSCRIPTION_STATUS_MAP = {
        INICIADA: u"Iniciada",
        PENDENTE: u"Pendente",
        ATIVA: u"Ativa",
        TROCANDO_PAGAMENTO: u"Alteração na forma de pagamento",
        SUSPENDED: u"Suspendida",
        CANCELLED: u"Cancelada",
        CANCELLED_BY_RECEIVER: u"Cancelada pela loja",
        CANCELLED_BY_SENDER: u"Cancelada pelo cliente",
        EXPIRED: u"Expirada"
    }

    ORDEM_PAGA = '5'
    NAO_PAGA = '6'
    PAYMENT_ORDER_STATUS_MAP = {
        '1': u"Programada",
        '2': u"Processando",
        '3': u"Não Processada",
        '4': u"Suspensa",
        ORDEM_PAGA: u"Paga",
        NAO_PAGA: u"Não Paga"
    }

    @staticmethod
    def get_transaction_status_display(status):
        u"""
        Mapeia o código de status de transação pro nome bonito.

        :type status: str
        :rtype: unicode
        """
        return PagSeguro.TRANSACTION_STATUS_MAP[status]

    @staticmethod
    def get_subscription_status_display(status):
        u"""
        Mapeia o código de status da assinatura pro nome bonito.

        :type status: str
        :rtype: unicode
        """
        return PagSeguro.SUBSCRIPTION_STATUS_MAP[status]

    @staticmethod
    def is_paid(instance):
        u"""
        Verifica se a transação está com estado de paga.

        :type instance: pagseguro_v2.parsers.PagSeguroNotificationResponse | pagseguro_v2.parsers.PagSeguroPreApproval
        """
        return instance.status == PagSeguro.PAGA

    @staticmethod
    def is_canceled(instance):
        u"""
        Verifica se a transação está com estado de cancelada.

        :type instance: pagseguro_v2.parsers.PagSeguroNotificationResponse | pagseguro_v2.parsers.PagSeguroPreApproval
        """
        return instance.status == PagSeguro.CANCELADA

    @staticmethod
    def is_active(instance):
        u"""
        Verifica se a assinatura está com estado de ativa.

        :type instance: pagseguro_v2.parsers.PagSeguroPreApproval
        """
        return instance.status == PagSeguro.ATIVA

    @staticmethod
    def is_canceled(instance):
        u"""
        Verifica se a assinatura está com estado de cancelada.

        :type instance: pagseguro_v2.parsers.PagSeguroPreApproval
        """
        return instance.status not in PagSeguro.NOT_CANCELED_STATUSES

    @staticmethod
    def parse_date(date):
        u"""
        Transformamos o objeto de data da API do PagSeguro para um valor válido tanto se for texto como um datetime.

        :type date: datetime | str
        :rtype: datetime
        """
        if isinstance(date, datetime):
            return date.replace(tzinfo=None)
        else:
            # Primeiro quebramos no - do nosso fuso e depois no . dos milisegundos se existir (API do PagSeguro é incosistente)
            return datetime.strptime(date.rsplit('-', 1)[0].split('.')[0], '%Y-%m-%dT%H:%M:%S')


class CheckoutItem(object):
    u""" Item do carrinho de compras. """

    def __init__(self, id, description, amount, quantity, weight=None):
        u"""
        Formato de um item de checkout.

        :param id: Identificador do produto. Precisa ser único dentro dos múltiplos itens de um pedido
        :type id: str
        :param description: Descrição do produto
        :type description: str
        :param amount: Valor do produto, e.g. '50.00'
        :type amount: str
        :param quantity: Quantidade deste produto
        :type quantity: int
        :param weight: Peso em gramas do item (máximo de 30000)
        :type weight: int
        """
        self.id = id
        self.description = description
        self.amount = amount
        self.quantity = quantity
        self.weight = weight


class Shipping(object):
    u""" Endereço de entrega de uma compra. """

    TYPE_PAC = 1
    TYPE_SEDEX = 2

    def __init__(self, type, postal_code, street=None, number=None, district=None, city=None, state=None, complement=None):
        u"""
        Formato de um endereço de entrega.

        Para criar um pedido com cálculo de frete automático é obrigatório apenas o CEP pois o resto do endereço é pedido pelo PagSeguro no pagamento.

        :param street: Tipo de entrega. `platforms.pagseguro.api.models.Shipping.TYPE_PAC` or `platforms.pagseguro.api.models.Shipping.TYPE_SEDEX`
        :type street: int
        :param street: Nome da rua
        :type street: str
        :param number: Número do local
        :type number: str
        :param district: Bairro
        :type district: str
        :param city: Cidade
        :type city: str
        :param state: Estado
        :type state: str
        :param postal_code: CEP apenas com números
        :type postal_code: str
        :param complement: Complemento opcional do local
        :type complement: str
        """
        self.type = type
        self.postal_code = postal_code
        self.street = street
        self.number = number
        self.complement = complement
        self.district = district
        self.city = city
        self.state = state


class PreApproval(object):
    u""" Pagamento com recorrência. """

    def __init__(self, charge, name, details, amount_per_payment, period):
        u"""
        Formato de uma recorrência.

        :param charge: se é uma recorrência de pagamento automático (feito pelo PagSeguro) ou manual (feito pelo cliente), podendo ser: AUTO, MANUAL.
        :type charge: str
        :param name: nome simples do serviço.
        :type name: str
        :param details:  descrição completa do serviço.
        :type details: str
        :param amount_per_payment: Quanto vai ser pago por recorrência, e.g. '50.00'
        :type amount_per_payment: str
        :param period: qual o tipo de recorrência, podendo ser: WEEKLY, MONTHLY, BIMONTHLY, TRIMONTHLY, SEMIANNUALLY, YEARLY.
        :type period: str
        """
        self.charge = charge
        self.name = name
        self.details = details
        self.amount_per_payment = amount_per_payment
        self.period = period
