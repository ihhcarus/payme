# -*- coding: utf-8 -*-


from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    url(r'^pagseguro/', include('pagseguro.urls', namespace='pagseguro')),
]
