# -*- coding: utf-8 -*-


from __future__ import absolute_import  # Deixa esse cara aqui pra não zicar o import de módulos com mesmo nome!

from datetime import datetime
from datetime import timedelta

from django.core.management import BaseCommand
from django.dispatch import receiver

from platforms.pagseguro.api.models import CheckoutItem, PreApproval, Shipping
from platforms.pagseguro.django.api import create_checkout, create_pre_approval, search_transactions_by_reference, search_transactions_by_date_range, update_pending_transactions, search_subscriptions_by_date_range, update_pending_subscriptions, update_active_subscriptions, get_subscriptions_payment_orders, update_subscriptions_payment_orders
from platforms.pagseguro.django.signals import checkout_created, pre_approval_created, pre_approval_transaction_created, checkout_transaction_created, pre_approval_transaction_paid, checkout_transaction_paid, subscription_created, subscription_canceled, subscription_active


class Command(BaseCommand):
    CRIAR_CHECKOUT_UM_ITEM = 'criar-checkout-um-item'
    CRIAR_CHECKOUT_MULTIPLOS_ITENS = 'criar-checkout-multiplos-itens'
    CRIAR_CHECKOUT_COM_SHIPPING = 'criar-checkout-shipping'

    CRIAR_RECORRENCIA = 'criar-recorrencia'

    BUSCAR_TRANSACOES_REFERENCIA = 'buscar-transacoes-referencia'
    BUSCAR_TRANSACOES_INTERVALO = 'buscar-transacoes-intervalo'
    BUSCAR_ASSINATURAS_INTERVALO = 'buscar-assinaturas-intervalo'
    BUSCAR_ORDENS_PGTO_ASSINATURA = 'buscar-ordens-pgto-assinatura'

    ATUALIZAR_TRANSACOES_PENDENTES = 'atualizar-transacoes-pendentes'
    ATUALIZAR_ASSINATURAS_PENDENTES = 'atualizar-assinaturas-pendentes'
    ATUALIZAR_ASSINATURAS_ATIVAS = 'atualizar-assinaturas-ativas'
    ATUALIZAR_ORDENS_PGTO = 'atualizar-ordens-pgto'

    NOME_DJANGO = u"Produto de Django"
    PRECO_DJANGO = '3.52'

    help = u"Testes da API do PagSeguro com Django."

    def add_arguments(self, parser):
        parser.add_argument(
            'django',
            type=str,
            choices=[
                self.CRIAR_CHECKOUT_UM_ITEM, self.CRIAR_CHECKOUT_MULTIPLOS_ITENS, self.CRIAR_CHECKOUT_COM_SHIPPING, self.CRIAR_RECORRENCIA,
                self.BUSCAR_TRANSACOES_REFERENCIA, self.BUSCAR_TRANSACOES_INTERVALO, self.BUSCAR_ASSINATURAS_INTERVALO, self.BUSCAR_ORDENS_PGTO_ASSINATURA,
                self.ATUALIZAR_TRANSACOES_PENDENTES, self.ATUALIZAR_ASSINATURAS_PENDENTES, self.ATUALIZAR_ASSINATURAS_ATIVAS, self.ATUALIZAR_ORDENS_PGTO
            ],
            help=u"Qual a funcionalidade da API deve ser testada."
        )

    def handle(self, *args, **options):
        django = options['django']
        print(u"executando chamada de: %s" % django)

        @receiver(checkout_created)
        def on_checkout_created(sender, instance, **kwargs):
            print u"checkout signal de %d" % instance.pk

        @receiver(pre_approval_created)
        def on_pre_approval_created(sender, instance, **kwargs):
            print u"pre approval signal de %d" % instance.pk

        @receiver(checkout_transaction_created)
        def on_checkout_transaction_created(sender, instance, **kwargs):
            print u"checkout transaction signal de %d" % instance.pk

        @receiver(pre_approval_transaction_created)
        def on_pre_approval_transaction_created(sender, instance, **kwargs):
            print u"pre approval transaction signal de %d" % instance.pk

        @receiver(checkout_transaction_paid)
        def on_checkout_transaction_paid(sender, instance, **kwargs):
            print u"paid checkout transaction signal de %d" % instance.pk

        @receiver(pre_approval_transaction_paid)
        def on_pre_approval_transaction_paid(sender, instance, **kwargs):
            print u"paid pre approval transaction signal de %d" % instance.pk

        @receiver(subscription_created)
        def on_subscription_created(sender, instance, **kwargs):
            print u"created subscription signal de %d" % instance.pk

        @receiver(subscription_active)
        def on_subscription_active(sender, instance, **kwargs):
            print u"active subscription signal de %d" % instance.pk

        @receiver(subscription_canceled)
        def on_subscription_canceled(sender, instance, **kwargs):
            print u"canceled subscription signal de %d" % instance.pk

        self.checkout(django)
        self.recorrencias(django)
        self.buscar(django)
        self.atualizar(django)

    def checkout(self, api):
        if api == self.CRIAR_CHECKOUT_UM_ITEM:
            checkout = create_checkout(CheckoutItem(**{
                'id': 'prod0001',  # Identificador do produto
                'description': self.NOME_DJANGO,  # Descrição do produto
                'amount': self.PRECO_DJANGO,  # Valor do produto
                'quantity': 1  # Quantidade de produtos
            }))
            print(u"transação criada: %s" % checkout.code)
        elif api == self.CRIAR_CHECKOUT_MULTIPLOS_ITENS:
            itens = [
                CheckoutItem(**{
                    'id': 'prod0001',  # Identificador do produto
                    'description': self.NOME_DJANGO,  # Descrição do produto
                    'amount': self.PRECO_DJANGO,  # Valor do produto
                    'quantity': 1  # Quantidade de produtos
                }),
                CheckoutItem(**{
                    'id': 'prod0002',  # Identificador do produto
                    'description': self.NOME_DJANGO,  # Descrição do produto
                    'amount': self.PRECO_DJANGO,  # Valor do produto
                    'quantity': 1  # Quantidade de produtos
                })
            ]
            checkout = create_checkout(itens)
            print(u"transação criada: %s" % checkout.code)
        elif api == self.CRIAR_CHECKOUT_COM_SHIPPING:
            checkout = create_checkout(CheckoutItem(**{
                'id': 'prod0001',  # Identificador do produto
                'description': self.NOME_DJANGO,  # Descrição do produto
                'amount': self.PRECO_DJANGO,  # Valor do produto
                'quantity': 1,  # Quantidade de produtos
                'weight': 2000  # Peso em gramas do item
            }), Shipping(**{
                'type': Shipping.TYPE_PAC,  # Tipo de entrega
                'street': u"Rua da República",  # Nome da rua
                'number': u"53",  # Número do local
                'complement': u"Apto 302",  # Complemento opcional do local
                'district': u"Cidade Baixa",  # Cidade
                'city': u"Porto Alegre",  # Estado
                'state': u"RS",  # CEP apenas com números
                'postal_code': u"90050321"  # Bairro
            }))
            print(u"transação criada: %s" % checkout.code)

    def recorrencias(self, api):
        if api == self.CRIAR_RECORRENCIA:
            pre_approval = create_pre_approval(PreApproval(**{
                'charge': 'auto',  # A cobrança é realizada automaticamente pelo PagSeguro (ao contrário de ter que realizar uma chamada toda recorrência)
                'name': self.NOME_DJANGO,  # Nome simples do serviço
                'details': u"Assinatura mensal do nosso serviço",  # Descrição completa do serviço
                'amount_per_payment': self.PRECO_DJANGO,  # Quanto vai ser pago por recorrência
                'period': 'MONTHLY'  # Qual o tipo de recorrência, podendo ser, WEEKLY, MONTHLY, BIMONTHLY, TRIMONTHLY, SEMIANNUALLY, YEARLY
            }))
            print(u"recorrência criada: %s" % pre_approval.code)

    def buscar(self, api):
        if api == self.BUSCAR_TRANSACOES_REFERENCIA:
            reference = 'OSWBLKCHNREVOEBOOK'
            checkout_transactions, pre_approval_transactions = search_transactions_by_reference(reference)
            print(u"transações de checkout para a referência \"%s\": %d" % (reference, len(checkout_transactions)))
            print(u"transações de recorrência para a referência \"%s\": %d" % (reference, len(pre_approval_transactions)))
        elif api == self.BUSCAR_TRANSACOES_INTERVALO:
            final_date = datetime.now()
            initial_date = final_date - timedelta(days=30)  # Pegamos dos últimos 30 dias
            checkout_transactions, pre_approval_transactions = search_transactions_by_date_range(initial_date, final_date)
            print(u"transações de checkout para o período de %s até %s: %d" % (initial_date.strftime('%Y-%m-%d %H:%M'), final_date.strftime('%Y-%m-%d %H:%M'), len(checkout_transactions)))
            print(u"transações de recorrência para o período de %s até %s: %d" % (initial_date.strftime('%Y-%m-%d %H:%M'), final_date.strftime('%Y-%m-%d %H:%M'), len(pre_approval_transactions)))
        elif api == self.BUSCAR_ASSINATURAS_INTERVALO:
            final_date = datetime.now()
            initial_date = final_date - timedelta(days=30)  # Pegamos dos últimos 30 dias
            subscriptions = search_subscriptions_by_date_range(initial_date, final_date)
            print(u"assinaturas para o período de %s até %s: %d" % (initial_date.strftime('%Y-%m-%d %H:%M'), final_date.strftime('%Y-%m-%d %H:%M'), len(subscriptions)))
        elif api == self.BUSCAR_ORDENS_PGTO_ASSINATURA:
            code = 'E94A2C48A5A5E72FF4EF9FA6683C5F81'
            response = get_subscriptions_payment_orders(code)
            print(u"ordens de pagamento de %s: %d" % (code, len(response)))

    def atualizar(self, api):
        if api == self.ATUALIZAR_TRANSACOES_PENDENTES:
            updated_checkout_transactions, updated_pre_approval_transactions = update_pending_transactions()
            print(u"transações de checkout atualizadas: %d" % len(updated_checkout_transactions))
            print(u"transações de recorrência atualizadas: %d" % len(updated_pre_approval_transactions))
        elif api == self.ATUALIZAR_ASSINATURAS_PENDENTES:
            updated_subscriptions = update_pending_subscriptions()
            print(u"assinaturas pendentes atualizadas: %d" % len(updated_subscriptions))
        elif api == self.ATUALIZAR_ASSINATURAS_ATIVAS:
            updated_subscriptions = update_active_subscriptions()
            print(u"assinaturas ativas atualizadas: %d" % len(updated_subscriptions))
        elif api == self.ATUALIZAR_ORDENS_PGTO:
            data_limite = datetime.now().replace(day=1, hour=0, minute=0) - timedelta(weeks=4)  # Limite de 1 mês atrás
            updated_payment_orders = update_subscriptions_payment_orders('E94A2C48A5A5E72FF4EF9FA6683C5F81', limit_date=data_limite)
            print(u"ordens de pagamento atualizadas: %d" % len(updated_payment_orders))
