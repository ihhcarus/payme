# -*- coding: utf-8 -*-


from __future__ import absolute_import  # Deixa esse cara aqui pra não zicar o import de módulos com mesmo nome!

from datetime import datetime, timedelta

from django.core.management import BaseCommand

from platforms.pagseguro.api.api import create_pre_approval, create_checkout, search_transaction_by_code, search_transactions_by_reference, search_transactions_by_date_range, search_subscription_by_code, search_subscriptions_by_date_range, get_subscriptions_payment_orders, cancel_subscription
from platforms.pagseguro.api.models import CheckoutItem, PreApproval, Shipping


class Command(BaseCommand):
    CRIAR_CHECKOUT_UM_ITEM = 'criar-checkout-um-item'
    CRIAR_CHECKOUT_MULTIPLOS_ITENS = 'criar-checkout-multiplos-itens'
    CRIAR_CHECKOUT_COM_SHIPPING = 'criar-checkout-shipping'
    BUSCAR_PAGAMENTO_CODIGO = 'buscar-pagamento-codigo'
    BUSCAR_PAGAMENTOS_REFERENCIA = 'buscar-pagamentos-referencia'
    BUSCAR_PAGAMENTOS_INTERVALO = 'buscar-pagamentos-intervalo'

    CRIAR_RECORRENCIA = 'criar-recorrencia'
    BUSCAR_ASSINATURA_CODIGO = 'buscar-assinatura-codigo'
    BUSCAR_ASSINATURA_INTERVALO = 'buscar-assinaturas-intervalo'
    BUSCAR_ORDENS_PGTO_ASSINATURA = 'buscar-ordens-pgto-assinatura'

    CANCELAR_ASSINATURA = 'cancelar-assinatura'

    NOME_API = u"Produto de API"
    PRECO_API = '2.74'

    help = u"Testes da API do PagSeguro."

    def add_arguments(self, parser):
        parser.add_argument(
            'api',
            type=str,
            choices=[
                self.CRIAR_CHECKOUT_UM_ITEM, self.CRIAR_CHECKOUT_MULTIPLOS_ITENS, self.CRIAR_CHECKOUT_COM_SHIPPING,
                self.BUSCAR_PAGAMENTO_CODIGO, self.BUSCAR_PAGAMENTOS_REFERENCIA, self.BUSCAR_PAGAMENTOS_INTERVALO,
                self.CRIAR_RECORRENCIA, self.BUSCAR_ASSINATURA_CODIGO, self.BUSCAR_ASSINATURA_INTERVALO, self.BUSCAR_ORDENS_PGTO_ASSINATURA,
                self.CANCELAR_ASSINATURA
            ],
            help=u"Qual a funcionalidade da API deve ser testada."
        )

    def handle(self, *args, **options):
        api = options['api']
        print(u"executando chamada de: %s" % api)
        self.checkouts(api)
        self.recorrencias(api)

    def checkouts(self, api):
        if api == self.CRIAR_CHECKOUT_UM_ITEM:
            response = create_checkout(CheckoutItem(**{
                'id': 'prod0001',  # Identificador do produto
                'description': self.NOME_API,  # Descrição do produto
                'amount': self.PRECO_API,  # Valor do produto
                'quantity': 1  # Quantidade de produtos
            }), reference_prefix='PROD%s', reference='PAGAMENTO')
            print(u"transação criada: %s" % response.code)
        elif api == self.CRIAR_CHECKOUT_MULTIPLOS_ITENS:
            itens = [
                CheckoutItem(**{
                    'id': 'prod0001',  # Identificador do produto
                    'description': self.NOME_API,  # Descrição do produto
                    'amount': self.PRECO_API,  # Valor do produto
                    'quantity': 1  # Quantidade de produtos
                }),
                CheckoutItem(**{
                    'id': 'prod0002',  # Identificador do produto
                    'description': self.NOME_API,  # Descrição do produto
                    'amount': self.PRECO_API,  # Valor do produto
                    'quantity': 1  # Quantidade de produtos
                })
            ]
            response = create_checkout(itens, reference_prefix='PROD%s', reference='PAGAMENTO')
            print(u"transação criada: %s" % response.code)
        elif api == self.CRIAR_CHECKOUT_COM_SHIPPING:
            response = create_checkout(CheckoutItem(**{
                'id': 'prod0001',  # Identificador do produto
                'description': self.NOME_API,  # Descrição do produto
                'amount': self.PRECO_API,  # Valor do produto
                'quantity': 1,  # Quantidade de produtos
                'weight': 2000  # Peso em gramas do item
            }), Shipping(**{
                'type': Shipping.TYPE_PAC,  # Tipo de entrega
                'street': u"Rua da República",  # Nome da rua
                'number': u"53",  # Número do local
                'complement': u"Apto 302",  # Complemento opcional do local
                'district': u"Cidade Baixa",  # Cidade
                'city': u"Porto Alegre",  # Estado
                'state': u"RS",  # CEP apenas com números
                'postal_code': u"90050321"  # Bairro
            }), reference_prefix='PROD%s', reference='PAGAMENTO')
            print(u"transação criada: %s" % response.code)
        elif api == self.BUSCAR_PAGAMENTO_CODIGO:
            response = search_transaction_by_code('73908F910D0DB7A884649F811097029D')
            if response:
                print(u"pagamento encontrado: %s" % response.code)
            else:
                print(u"pagamento não encontrado...")
        elif api == self.BUSCAR_PAGAMENTOS_REFERENCIA:
            reference = 'MU'
            response = search_transactions_by_reference(reference)
            print(u"pagamentos para a referência \"%s\": %d" % (reference, len(response)))
        elif api == self.BUSCAR_PAGAMENTOS_INTERVALO:
            final_date = datetime.now()
            initial_date = final_date - timedelta(days=30)  # Pegamos dos últimos 30 dias
            response = search_transactions_by_date_range(initial_date, final_date)
            print(u"pagamentos para o período de %s até %s: %d" % (initial_date.strftime('%Y-%m-%dT%H:%M'), final_date.strftime('%Y-%m-%dT%H:%M'), len(response)))

    def recorrencias(self, api):
        if api == self.CRIAR_RECORRENCIA:
            response = create_pre_approval(PreApproval(**{
                'charge': 'auto',  # A cobrança é realizada automaticamente pelo PagSeguro (ao contrário de ter que realizar uma chamada toda recorrência)
                'name': self.NOME_API,  # Nome simples do serviço
                'details': u"Assinatura mensal do nosso serviço",  # Descrição completa do serviço
                'amount_per_payment': self.PRECO_API,  # Quanto vai ser pago por recorrência
                'period': 'MONTHLY',  # Qual o tipo de recorrência, podendo ser, WEEKLY, MONTHLY, BIMONTHLY, TRIMONTHLY, SEMIANNUALLY, YEARLY
            }))
            print(u"recorrência criada: %s" % response.code)
        elif api == self.BUSCAR_ASSINATURA_CODIGO:
            response = search_subscription_by_code('3CD879E638385E60040D7FB4BB4B1E7F')
            if response:
                print(u"assinatura encontrada: %s" % response.code)
            else:
                print(u"assinatura não encontrada...")
        elif api == self.BUSCAR_ASSINATURA_INTERVALO:
            final_date = datetime.now()
            initial_date = final_date - timedelta(days=30)  # Pegamos dos últimos 30 dias
            response = search_subscriptions_by_date_range(initial_date, final_date)
            print(u"assinaturas para o período de %s até %s: %d" % (initial_date.strftime('%Y-%m-%dT%H:%M'), final_date.strftime('%Y-%m-%dT%H:%M'), len(response)))
        elif api == self.BUSCAR_ORDENS_PGTO_ASSINATURA:
            code = '41D68386040401133448DFBF506E6199'
            response = get_subscriptions_payment_orders(code)
            print(u"ordens de pagamento de %s: %d" % (code, len(response)))
        elif api == self.CANCELAR_ASSINATURA:
            code = '022F6AA9767614BCC4B13FB3A4DA2895'
            response = cancel_subscription(code)
            if response.canceled:
                print(u"assinatura %s cancelada com sucesso" % code)
            else:
                print(u"não foi possível cancelar a assinatura %s" % code)
