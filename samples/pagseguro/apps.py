# -*- coding: utf-8 -*-


from django.apps import AppConfig


class SamplesPagSeguroConfig(AppConfig):
    name = 'samples.pagseguro'
    label = 'samples_pagseguro'
    verbose_name = u"PagSeguro Samples"
