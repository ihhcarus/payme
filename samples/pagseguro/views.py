# -*- coding: utf-8 -*-


from django.conf import settings
from django.contrib import messages
from django.urls.base import reverse_lazy
from django.views.generic import TemplateView

from platforms.pagseguro.api.api import search_transaction_by_code
from platforms.pagseguro.django.mixins import PaymentAndPreApprovalMixin, TransactionMixin, TransactionFromReferenceMixin, SubscriptionCancelView


class PagSeguroDemoView(PaymentAndPreApprovalMixin, TransactionFromReferenceMixin, TemplateView):
    u""" Mostra os botões de integração com PagSeguro e um campo para busca de uma transação. """

    template_name = 'demo.html'

    payment_code = settings.PAGSEGURO_CODE_PAYMENT
    pre_approval_code = settings.PAGSEGURO_CODE_PRE_APPROVAL

    transaction_details = None

    def post(self, request, *args, **kwargs):
        transaction = request.POST.get('transaction')
        if transaction:
            self.transaction_details = search_transaction_by_code(transaction).xml
        return super(PagSeguroDemoView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(PagSeguroDemoView, self).get_context_data(**kwargs)
        context_data['transaction_details'] = self.transaction_details
        context_data['subscription_pk'] = 20
        return context_data


class ThankYouView(TransactionMixin, TemplateView):
    u""" View de redirecionamento após um pagamento, apenas mostra as informações do pagamento concluído. """

    template_name = 'thankyou.html'

    transaction_url_query = 'transaction_id'


class CancelarAssinaturaView(SubscriptionCancelView):
    u""" Mostra o cancelamento de uma assinatura. """

    template_name = 'cancelar.html'
    success_url = reverse_lazy('pagseguro:demo')

    def on_canceled(self):
        u""" Adicionamos uma mensagem de sucesso. """
        messages.success(self.request, u"Cancelado com sucesso.")
        return super(CancelarAssinaturaView, self).on_canceled()

    def on_not_canceled(self):
        u""" Adicionamos uma mensagem de erro. """
        messages.error(self.request, u"Não conseguiu cancelar.")
        return super(CancelarAssinaturaView, self).on_not_canceled()
