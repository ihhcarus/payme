# -*- coding: utf-8 -*-


from django.conf.urls import url

from samples.pagseguro.views import PagSeguroDemoView, ThankYouView, CancelarAssinaturaView


urlpatterns = [
    url(r'^demo/$', PagSeguroDemoView.as_view(), name='demo'),
    url(r'^obrigado/$', ThankYouView.as_view(), name='thankyou'),
    url(r'^cancelar/(?P<pk>\d+)/$', CancelarAssinaturaView.as_view(), name='cancelar'),
]
